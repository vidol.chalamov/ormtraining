﻿using Microsoft.EntityFrameworkCore;
using ORMTraining.Entities;
using System.Collections.Generic;
using System.Text;

namespace ORMTraining
{
    public class PetsContext : DbContext
    {
        public DbSet<Dog> Dogs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=localhost\SQLEXPRESS;Database=Pets;Integrated Security=True");
        }
    }
}
