﻿using ORMTraining.Entities;
using System.Threading.Tasks;

namespace ORMTraining
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var repo = new DogsRepository();

            // await repo.AddAsync(new Dog { Name = "Rex" });

            // var dogId = Guid.Parse("AD2EF7A8-40AC-4C19-E263-08D7F24B3162")

            // await repo.UpdateAsync(new Dog
            // {
            //     Id = dogId,
            //     Name = "Rex The Goodest boi"
            // });

            // var dogs = await repo.GetPaginatedAsync(1, 10);
            // 
            // foreach (var dog in dogs)
            // {
            //     Console.WriteLine($"{dog.Id} has the name {dog.Name}");
            // }

            // var dog = await repo.GetByIdAsync(dogId);
            // 
            // if (dog != null)
            // {
            //     Console.WriteLine($"{dog.Id} has the name {dog.Name}");
            // }

            // await repo.DeleteAsync(dogId);
        }
    }
}
