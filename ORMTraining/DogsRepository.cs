﻿using Microsoft.EntityFrameworkCore;
using ORMTraining.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ORMTraining
{
    public class DogsRepository
    {
        public async Task<IEnumerable<Dog>> GetPaginatedAsync(int page, int size)
        {
            using (var db = new PetsContext())
            {
                return await db.Dogs
                    .Skip((page - 1) * size)
                    .Take(size)
                    .ToListAsync();
            }
        }

        public async Task<Dog> GetByIdAsync(Guid id)
        {
            using (var db = new PetsContext())
            {
                return await db.Dogs.FindAsync(id);
            }
        }

        public async Task AddAsync(Dog dog)
        {
            using (var db = new PetsContext())
            {
                await db.Dogs.AddAsync(dog);

                await db.SaveChangesAsync();
            }
        }

        public async Task UpdateAsync(Dog dog)
        {
            using (var db = new PetsContext())
            {
                db.Dogs.Update(dog);

                await db.SaveChangesAsync();
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            using (var db = new PetsContext())
            {
                var dog = await db.Dogs.FindAsync(id);

                if (dog != null)
                {
                    db.Dogs.Remove(dog);
                    await db.SaveChangesAsync();
                }
            }
        }
    }
}
