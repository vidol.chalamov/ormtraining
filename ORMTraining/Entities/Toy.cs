﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ORMTraining.Entities
{
    public class Toy
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Guid DogId { get; set; }
    }
}
