﻿using Microsoft.EntityFrameworkCore;
using ORMTraining.Web.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ORMTraining.Web
{
    public class DogsRepository : BaseRepository<Dog>
    {
        public DogsRepository(PetsContext context): base(context)
        {
        }
    }
    public class ToysRepository : BaseRepository<Toy>
    {
        public ToysRepository(PetsContext context) : base(context)
        {
        }
    }
}
