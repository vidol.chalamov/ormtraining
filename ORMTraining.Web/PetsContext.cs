﻿using Microsoft.EntityFrameworkCore;
using ORMTraining.Web.Entities;

namespace ORMTraining.Web
{
    public class PetsContext : DbContext
    {
        public PetsContext(DbContextOptions<PetsContext> options) : base(options)
        {
        }

        public DbSet<Dog> Dogs { get; set; }
        public DbSet<Toy> Toys { get; set; }
    }
}
