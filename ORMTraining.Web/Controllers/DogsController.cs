﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ORMTraining.Web.Entities;

namespace ORMTraining.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DogsController : ControllerBase
    {
        private readonly DogsRepository _dogsRepository;
        private readonly ToysRepository _toysRepository;

        public DogsController(DogsRepository dogsRepository, ToysRepository toysRepository)
        {
            _dogsRepository = dogsRepository;
            _toysRepository = toysRepository;
        }

        [HttpGet]
        public async Task<IEnumerable<Dog>> GetAsync()
        {
            return await _dogsRepository.GetPaginatedAsync(1, 10);
        }
    }
}
